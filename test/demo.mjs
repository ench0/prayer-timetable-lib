/* eslint-disable no-unused-vars */
import colors from 'colors'
import format from 'date-fns/format'

import { prayersCalc } from './index'
import timetable from './timetable'
import settings from './settings'
import { hijriDate, convertToDuration } from './func'

const appendZero = (n) => (n < 10 ? `0${n}` : `${n}`)

const exec = () => {
  const {
    prayers,
    previous,
    current,
    next,
    countUp,
    countDown,
    now,
    hijri,
    percentage,
    isAfterIsha,
    isJamaahPending,
    focus,
  } = prayersCalc(timetable, settings)
  process.stdout.write('\x1B[2J')

  const prayerList = isAfterIsha ? prayers.tomorrow : prayers.today

  console.log(
    format(now, 'dd/MMM/yyyy').yellow.bold,
    hijriDate(hijri),
    ` | dst: ${prayerList[0].dstAdjust} | `,
    'isafterIsha:',
    isAfterIsha,
    ' | isJamaahPending:',
    isJamaahPending,
    ' | focus:',
    focus.name,
    '\n'
  )
  prayerList.forEach((element) => {
    console.log(
      element.when,
      '\t',
      //   element.time,
      `${element.index} - ${element.name.green} \t ${format(element.time, 'HH:mm').blue}${
        `\t${format(element.jtime, 'HH:mm')}`.cyan
      } ${`\t${element.isNext}`.cyan} ${focus.name === element.name ? 'next'.grey : ''} ${
        element.isJamaahPending ? 'jamaah pending'.grey : ''
      }`
    )
  })

  console.log('')

  console.log(
    `${countUp.name} ${format(countUp.time, 'HH:mm')}`.cyan,
    `${convertToDuration(countUp.duration)}`.magenta,
    `<< ${format(now, 'HH:mm:ss')} >>`.yellow.bold,
    `${convertToDuration(countDown.duration)}`.magenta,
    `${countDown.name} ${format(countDown.time, 'HH:mm')}`.cyan
  )

  console.log('')
  console.log(
    `${'█'.yellow.repeat(Math.ceil(percentage / 2))}${'█'.grey.repeat(
      Math.floor((100 - percentage) / 2)
    )} ${percentage}`
  )
}

let timer
const tick = () => {
  /* eslint-disable no-use-before-define */
  clearInterval(timer)
  timer = setInterval(() => exec(), 1000)
}

tick()
